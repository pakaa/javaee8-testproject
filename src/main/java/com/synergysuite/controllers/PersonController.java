package com.synergysuite.controllers;

import com.synergysuite.jpa.PersonBean;
import com.synergysuite.services.PersonService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/person")
public class PersonController {

    @Inject
    private PersonService personService;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public PersonBean createPerson(@QueryParam("name") String name, @QueryParam("lastName") String lastName, @QueryParam("age") Integer age) {
        return personService.createPerson(name, lastName, age);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public PersonBean fetchPerson(@QueryParam("name") String name) {
        return personService.findPerson(name);
    }

}