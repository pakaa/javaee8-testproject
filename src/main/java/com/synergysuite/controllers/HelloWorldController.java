package com.synergysuite.controllers;

import com.synergysuite.services.HelloWorldService;

import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("/hello")
public class HelloWorldController {

    @Inject
    private HelloWorldService helloWorldService;

    @GET
    public String helloWorld(@QueryParam("name") @DefaultValue("World") String name, @QueryParam("formal") @DefaultValue("false") boolean formal) {
        return helloWorldService.generateGreeting(name, formal);
    }

}