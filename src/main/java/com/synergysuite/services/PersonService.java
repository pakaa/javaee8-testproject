package com.synergysuite.services;

import com.synergysuite.jpa.PersonBean;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class PersonService {

    @PersistenceContext(name = "TestPU")
    private EntityManager em;

    public PersonBean createPerson(String name, String lastName, Integer age) {
        PersonBean person = new PersonBean();
        person.setName(name);
        person.setLastName(lastName);
        person.setAge(age);

        em.persist(person);
        return person;
    }

    public PersonBean findPerson(String name) {
        return em.createQuery("SELECT p FROM PersonBean p WHERE p.name = :name", PersonBean.class)
                .setParameter("name", name)
                .getResultList().stream().findFirst().orElse(null);
    }

}
