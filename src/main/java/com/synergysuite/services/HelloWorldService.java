package com.synergysuite.services;

import javax.ejb.Stateless;

@Stateless
public class HelloWorldService {

    public String generateGreeting(String name, boolean formal) {
        if (formal) {
            return "Greetings " + name;
        } else {
            return "Hello " + name;
        }
    }

}
